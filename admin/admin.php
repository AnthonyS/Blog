<?php
require_once('../include/config.php');

if (isset($_POST["username"]) && isset($_POST["password"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    $sql = "SELECT password, idadminUser FROM adminUser WHERE username = :username";
    $stmt = $dbh->prepare($sql);      // sert à mettre la valeur d'une variable dans une requête SQL de façon sécurisée.
    $stmt->bindValue(':username', $username);
    $stmt->execute();
    $result = $stmt->fetch();


    if (crypt($password, $salt) == $result[0]) {   // check le mdp
        $_SESSION['isAdmin'] = true;
        $_SESSION['authUser'] = $username;
        $_SESSION['idadminUser'] = $result[1];
    }
}

    if ($_SESSION['isAdmin']) {
        echo "Bienvenue " . $_SESSION['authUser'];
    }else {
        echo "Vous n'êtes pas autorisez !";
    }

    $posts = [];
    $sql = "SELECT
                p.id as id,
                p.title as title,
                aU.username as adminUser,
                c.name as category,
                p.date as date
            FROM
                post p
            LEFT JOIN adminUser aU
            ON p.FK_adminUser = aU.id
            LEFT JOIN category c
            ON p.FK_category = c.id;";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
    $posts = $stmt->fetchAll();

  ?>

  <!DOCTYPE html>
  <html lang="fr" dir="ltr">
    <head>
      <meta charset="utf-8">
    </head>
    <body>
      <?php
    echo "<h2>Bienvenue " . $_SESSION['authUser']."</h2> ";
?>
<div>
    <table>
        <table style="width:100%">
          <tr class="table-first-line">
            <th>Auteur</th>
            <th>Titre</th>
            <th>Catégorie</th>
            <th>Date</th>
          </tr>
          <?php
            foreach ($posts as $post) {
                echo "<tr>
                        <td>".$post["adminUser"]."</td>
                        <td>".$post["title"]."</td>
                        <td>".$post["category"]."</td>
                        <td>".$post["date"]."</td>
                    </tr>";
            }
          ?>
    </table>
</div>
<div>
    <a href="/my_blog/admin/redactor.php" target="_blank">Créer un nouvel article</a>
</div>
    </body>
  </html>
