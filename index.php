<?php
    require_once('include/config.php');
    $posts = [];
    $sql = "SELECT
                p.id as id,
                p.title as title,
                aU.username as adminUser,
                c.name as category,
                p.date as date,
                p.content as content
            FROM
                post p
            LEFT JOIN adminUser aU
            ON p.FK_adminUser = aU.id
            LEFT JOIN category c
            ON p.FK_category = c.id
            ORDER BY date DESC;";
    $stmt = $dbh->prepare($sql);
    $stmt->execute();
    $posts = $stmt->fetchAll();

?>

<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>La Skat'style</title>
  </head>
  <body>
    <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <?php
                            foreach ($posts as $post) {
                                echo
                                "<div class='wrap-post'>
                                    <div class='header-post'>
                                        <div class='title-post'>".$post["title"]."
                                            <div class='info-post'>".$post["category"].", par ".$post["adminUser"]." le ".$post["date"]."</div>
                                        </div>
                                    </div>
                                    <div class='content-post'>
                                        <div class='content'>".$post["content"]."</div>
                                        <div class='fade-content'><a href='/my_lazy_blog/article.php?p=".$post["id"]."'>Lire la suite</a></div>
                                   </div>
                                </div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
  </body>
</html>
